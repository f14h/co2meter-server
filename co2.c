#include <stdlib.h>             // for exit, EXIT_FAILURE, free, malloc
#include <stdio.h>              // for fprintf, perror, stderr, NULL
#include <stdbool.h>            // for bool, false, true

#include <inttypes.h>			// for uint16_t, PRIu16
#include <unistd.h>             // for close, unlink, ssize_t
#include <time.h>				// for time
#include <signal.h>				// for signal, SIGINT, SIGTERM
#include <string.h>				// for strlen, memset, strcpy
#include <assert.h>				// for assert
#include <pthread.h>			// for pthread_create, pthread_join

#include <sys/socket.h>			// for accept, bind, listen, send, shu...
#include <sys/un.h>				// for sockaddr_un

#include <libusb-1.0/libusb.h>	// for libusb_*
#include <linux/hid.h>			// for HID_REQ_SET_REPORT

#define CHECK(errorcode) ({ \
	if (0 > (errorcode)) { \
		fprintf(stderr, "%s:%d: ", \
			__FILE__, __LINE__ \
		); \
		perror("errorcode failed"); \
		exit(EXIT_FAILURE); \
	} \
})

#define CHECK_LIBUSB(errorcode) ({ \
	if (0 > (errorcode)) { \
		fprintf(stderr, "%s:%d: libusb error: %s (%s)\n", \
			__FILE__, __LINE__, \
			libusb_error_name((errorcode)), \
			libusb_strerror((errorcode)) \
		); \
		exit(EXIT_FAILURE); \
	} \
})

#define IGNORE_LIBUSB(ignore, errorcode) ({ \
	int r_IGNORE_LIBUSB = (errorcode); \
	((ignore) == r_IGNORE_LIBUSB) ? 0 : r_IGNORE_LIBUSB; \
})

#define CO2_METER_VENDOR 		0x04d9
#define CO2_METER_PRODUCT		0xa052

// Item codes. See http://co2meters.com/Documentation/Other/AN_RAD_0301_USB_Communications_Revised8.pdf
#define CO2_METER_ITEM_CntR		0x50
#define CO2_METER_ITEM_Tamb		0x42
#define CO2_METER_ITEM_Hdty		0x44

#define CO2_METER_ENDPOINT_IN 	0x81

#define CO2_METER_FRAME_SIZE	8

#define CO2_METER_PIPE_BASENAME	"/tmp/co2meter_"
#define CO2_METER_PIPE_TEMP		"temp"
#define CO2_METER_PIPE_CO2		"co2"
#define CO2_METER_PIPE_RH		"rh"

// HID Class-Specific Requests values. See section 7.2 of the HID specifications
#define HID_REPORT_TYPE_INPUT         0x01
#define HID_REPORT_TYPE_OUTPUT        0x02
#define HID_REPORT_TYPE_FEATURE       0x03

struct thread_data_t
{
	int socket_fd;
	unsigned char op;
	uint16_t data;
};

static void sig_handler(int _);
static bool co2meter_find(const unsigned char key[8], libusb_device_handle **dev_handle);
static void co2meter_decrypt(const unsigned char key[CO2_METER_FRAME_SIZE], unsigned char data[CO2_METER_FRAME_SIZE]);
static void *co2meter_srv_sockets(struct thread_data_t *data);

static void *emalloc(size_t size) __attribute__((malloc));

static volatile sig_atomic_t keep_running = 1;

int main(void)
{
	bool ret;
	unsigned char data[CO2_METER_FRAME_SIZE] = { 0 };
	unsigned char key[CO2_METER_FRAME_SIZE] = { 0 };
	int transferred; 
	libusb_device_handle *dev_handle;	
	
	const char * const sockets[] = {
		CO2_METER_PIPE_BASENAME CO2_METER_PIPE_TEMP ".sock", 
		CO2_METER_PIPE_BASENAME CO2_METER_PIPE_CO2 ".sock"
/*		CO2_METER_PIPE_BASENAME CO2_METER_PIPE_RH ".sock"*/
	};
	const unsigned char ops[] = {
		CO2_METER_ITEM_Tamb, 
		CO2_METER_ITEM_CntR,
		CO2_METER_ITEM_Hdty
	};
	const size_t num_socks = sizeof sockets / sizeof *sockets;
	int socket_fd[num_socks];
	struct sockaddr_un sock_addr[num_socks];
	struct thread_data_t *co2meter_values = emalloc(num_socks * sizeof *co2meter_values);
	pthread_t srv_thread[num_socks];
	
	signal(SIGINT, sig_handler);
	signal(SIGTERM, sig_handler);
	
	// pseudo random key
	srand((unsigned int) time(NULL));
	for (int i = 0; CO2_METER_FRAME_SIZE > i; i++)
		key[i] = rand();
	
	for (size_t i = 0; i < num_socks; i++)
	{
		assert(strlen(sockets[i]) < sizeof(sock_addr[i].sun_path));
		
		socket_fd[i] = socket(PF_UNIX, SOCK_STREAM, 0);
		CHECK(socket_fd[i]);
		
		memset(&sock_addr[i], 0, sizeof(struct sockaddr_un));
		sock_addr[i].sun_family = AF_UNIX;
		strcpy(sock_addr[i].sun_path, sockets[i]);

		CHECK(bind(socket_fd[i], (struct sockaddr *) &sock_addr[i], sizeof(struct sockaddr_un)));
		CHECK(listen(socket_fd[i], 50));
		
		co2meter_values[i].socket_fd = socket_fd[i];
		co2meter_values[i].op = ops[i];
		
		CHECK(pthread_create(&srv_thread[i], NULL, (void * (*)(void *)) &co2meter_srv_sockets, &co2meter_values[i]));
	}
	
	CHECK_LIBUSB(libusb_init(NULL));
/*	CHECK_LIBUSB(libusb_set_option(NULL, LIBUSB_OPTION_LOG_LEVEL, LIBUSB_LOG_LEVEL_DEBUG)); */
	
	ret = co2meter_find(key, &dev_handle);
	if (!ret)
	{
		fprintf(stderr, "No CO2-Meter found!\n");
		
		libusb_exit(NULL);
		return EXIT_FAILURE;
	}
	
	while(keep_running)
	{
		CHECK_LIBUSB(IGNORE_LIBUSB(LIBUSB_ERROR_TIMEOUT, libusb_interrupt_transfer(dev_handle, CO2_METER_ENDPOINT_IN, data, sizeof(data), &transferred, 0)));
		co2meter_decrypt(key, data);
		
		if (transferred < 5)
		{
			fprintf(stderr, "Device sent less bytes than expected (%d, 5)!\n", transferred);
			continue;
		}
		
		if (((data[0] + data[1] + data[2]) & 0xff) != data[3])
		{
			fprintf(stderr, "Checksum mismatch!\n");
			continue;
		}
		
		for (size_t i = 0; i < num_socks; i++)
		{
			if (data[0] == ops[i])
			{
				co2meter_values[i].data = (data[1] << 8) | data[2];
			}
		}
	}
	
	CHECK_LIBUSB(libusb_release_interface(dev_handle, 0));
	CHECK_LIBUSB(libusb_attach_kernel_driver(dev_handle, 0));
	libusb_close(dev_handle);

	libusb_exit(NULL);
	memset(key, 0, sizeof(key));
	
	for (size_t i = 0; i < num_socks; i++)
	{
		CHECK(shutdown(socket_fd[i], SHUT_RDWR));
		CHECK(unlink(sockets[i]));
		
		pthread_join(srv_thread[i], NULL);
	}
	free(co2meter_values);
	
	return EXIT_SUCCESS;
}

static void sig_handler(int _)
{
    (void)_;
    keep_running = 0;
}

static bool co2meter_find(const unsigned char key[8], libusb_device_handle **dev_handle)
{
	int i = 0, r;
	ssize_t cnt = 0;
	bool ret = false;
	struct libusb_device_descriptor desc;
	libusb_device **devs;
	libusb_device *dev;
	
	cnt = libusb_get_device_list(NULL, &devs);
	CHECK_LIBUSB(cnt);
	
	while (NULL != (dev = devs[i++]))
	{	
		CHECK_LIBUSB(libusb_get_device_descriptor(dev, &desc));
		
		if (CO2_METER_VENDOR == desc.idVendor && CO2_METER_PRODUCT == desc.idProduct)
		{
			CHECK_LIBUSB(libusb_open(dev, dev_handle));
			
			r = libusb_kernel_driver_active(*dev_handle, 0);
			CHECK_LIBUSB(r);
			if (r) CHECK_LIBUSB(libusb_detach_kernel_driver(*dev_handle, 0));
			CHECK_LIBUSB(libusb_set_configuration(*dev_handle, 1));
			
			CHECK_LIBUSB(libusb_control_transfer(
				*dev_handle, HID_REPORT_TYPE_FEATURE
				LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE,
				HID_REQ_SET_REPORT,
				(HID_REPORT_TYPE_FEATURE << 8) | 0x00,
				0,
				(unsigned char *) key,
				CO2_METER_FRAME_SIZE,
				100
			));
			
			CHECK_LIBUSB(libusb_claim_interface(*dev_handle, 0));
			
			ret = true;
			break;
		}
	}
	
	libusb_free_device_list(devs, 1);
	
	return ret;
}

/* Adapted from https://hackaday.io/project/5301-reverse-engineering-a-low-cost-usb-co-monitor/log/16905-its-you */
static void co2meter_decrypt(const unsigned char key[8], unsigned char data[8])
{
	const unsigned char cstate[CO2_METER_FRAME_SIZE] = {0x84, 0x47, 0x56, 0xD6, 0x07, 0x93, 0x93, 0x56};
	const unsigned char shuffle[CO2_METER_FRAME_SIZE] = {2, 4, 0, 7, 1, 6, 5, 3};
	
	unsigned char phases[3][CO2_METER_FRAME_SIZE];
	
	for (int i = 0; CO2_METER_FRAME_SIZE > i; i++)
		phases[0][shuffle[i]] = data[i];
	
	for (int i = 0; CO2_METER_FRAME_SIZE > i; i++)
		phases[1][i] = phases[0][i] ^ key[i];
	
	for (int i = 0; CO2_METER_FRAME_SIZE > i; i++)
		phases[2][i] = ((phases[1][i] >> 3) | (phases[1][(i-1+8) % 8] << 5)) & 0xff;
	
	for (int i = 0; CO2_METER_FRAME_SIZE > i; i++)
		data[i] = (phases[2][i] - cstate[i]) & 0xff;
}

static void *co2meter_srv_sockets(struct thread_data_t *data)
{
	int socket;
	ssize_t ret;
	char buf[63];
	
	while (keep_running)
	{
		socket = accept(data->socket_fd, NULL, NULL);
		if (!keep_running) break;
		
		switch (data->op)
		{
			case CO2_METER_ITEM_CntR:
				snprintf(buf, sizeof(buf), "%" PRIu16, data->data);
				break;
				
			case CO2_METER_ITEM_Tamb:
			{
				float cels = data->data/16.0 - 273.15;
				snprintf(buf, sizeof(buf), "%2.2f %2.2f", cels, cels * 9.0 / 5.0 + 32.0);
			}	break;
				
			case CO2_METER_ITEM_Hdty:
				snprintf(buf, sizeof(buf), "%2.2f", data->data/100.0);
				break;
				
			default:
				buf[0] = 0;
				break;
		}
		
		ret = send(socket, buf, strlen(buf), 0);
		CHECK(ret);
		
		CHECK(close(socket));
	}
	
	return NULL;
}

// ===================================

static void *emalloc(size_t size)
{
	void *p = malloc(size);
	if (p == NULL)
	{
		perror("malloc failed");
		
		exit(EXIT_FAILURE);
	}
	
	return p;
}
