Simple, small server for co2meters.

Read data with `nc -U /tmp/co2meter_{temp,co2,th}.sock`
Compile with `clang -pthreads -lusb-1.0 co2.c -o co2`